package com.example.sagar.compassfinal


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory



/**
 * A class for api calls
 * @author Sagar Bhatnagar
 * @Date 8-Mar-2018
 */
class ApiClient {



    companion object {
        var client : OkHttpClient? = null


        var retrofit: Retrofit? = null
            get() {
                client = OkHttpClient.Builder().addInterceptor(interceptor).build()

                field = field ?: Retrofit.Builder()
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .baseUrl(BuildConfig.API_URL)
                        .client(client)
                        .build()
                return field
            }
            set(value) {
                field = value
            }



        private var gson : Gson? = null
            get() {
                field = field ?: GsonBuilder().create()
                return field
            }
            set(value) {
                field = value
            }

        private var interceptor = HttpLoggingInterceptor()
            get() {
                if (BuildConfig.DEBUG)
                    field.level = HttpLoggingInterceptor.Level.BODY
                else
                    field.level = HttpLoggingInterceptor.Level.NONE
                return field
            }
            set(value) {
                field = value
            }



    }

}