package com.example.sagar.compassfinal

import android.text.TextUtils
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class AppRepository private constructor() {

    companion object {
        fun getLocation( callback: ApiCallback<LocationResponse>) {
            val retrofit = ApiClient.retrofit
            val callPost = retrofit?.create<ApiService.LocationService>(ApiService.LocationService::class.java!!)?.location()

            callPost?.enqueue(object : Callback<LocationResponse> {
                override fun onResponse(call: Call<LocationResponse>?, response: Response<LocationResponse>?) {
                    if (response!!.isSuccessful) {
                        callback.onSuccess(response.body())
                    }
                }

                override fun onFailure(call: Call<LocationResponse>, t: Throwable) {
                    callback.onException(t)
                }
            })
        }

    }
}