import com.example.sagar.compassfinal.LocationResponse
import retrofit2.Call
import retrofit2.http.*

/**
 * Api Service
 * @author Sagar Bhatnagar
 * @Date 8-Mar-2018
 */

interface ApiService {

    interface LocationService {
        @GET("/")
        fun location(): Call<LocationResponse>
    }
}