package com.example.sagar.compassfinal

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.text.SpannableStringBuilder
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import com.example.sagar.compassfinal.databinding.ActivityCompBinding
import kotlinx.android.synthetic.main.activity_comp.*
import android.support.design.widget.BottomSheetDialog
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.bottom_sheet_layout.*
import kotlinx.android.synthetic.main.bottom_sheet_layout.view.*


class CompassActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var mSensorManager: SensorManager
    private lateinit var compassViewModel: CompassViewModel
    private lateinit var mBinding: ActivityCompBinding
    private var currentDegree = 0f
    var dialog: BottomSheetDialog? = null
    var destinationDegree: Double = 0.0
    val degree : Float = 0f


    var constaint: ConstraintSet = ConstraintSet()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_comp)
        compassViewModel = ViewModelProviders.of(this).get(CompassViewModel::class.java)
        mBinding.compassVm = compassViewModel
        compassViewModel.getLocation()
        mBinding.executePendingBindings()
        observeApiResponse()
        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager


        bt_pull_from_api.setOnClickListener(object :View.OnClickListener{
            override fun onClick(p0: View?) {
                compassViewModel.getLocationApi()
            }


        })

        if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) == null) {

            val builder = AlertDialog.Builder(this@CompassActivity)
            builder.setMessage("This Device is not compatible")
            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
        }

        bt_finder.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                inflateBottomSheet()
            }


        })

    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {

        // get the angle around the z-axis rotated
        val degree = Math.round(event!!.values[0]).toFloat()

        mBinding.tvHeading.text = "Heading: " + java.lang.Float.toString(degree) + " degrees"

        // create a rotation animation (reverse turn degree degrees)
        val ra = RotateAnimation(
                currentDegree,
                -degree,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f)

        // how long the animation will take place
        ra.duration = 210

        // set the animation after the end of the reservation status
        ra.fillAfter = true

        // Start the animation
        mBinding.imageViewCompass.startAnimation(ra)

        currentDegree = -degree

        constaint.clone(parents)

        constaint.constrainCircle(texts.getId(), imageViewCompass.getId(), 360, destinationDegree.toFloat() - degree );
        constaint.constrainCircle(ll_location.id,imageViewCompass.id,360,compassViewModel.apiDegree.toFloat() - degree )
        constaint.applyTo(parents)
    }


    override fun onResume() {
        super.onResume()

        // for the system's orientation sensor registered listeners
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                SensorManager.SENSOR_DELAY_GAME)
    }

    override fun onPause() {
        super.onPause()

        // to stop the listener and save battery
        mSensorManager.unregisterListener(this)
    }

    private fun inflateBottomSheet() {
        var isValid: Boolean = true
        dialog = BottomSheetDialog(this)
        val view: View = layoutInflater.inflate(R.layout.bottom_sheet_layout, null)
        view.visibility = View.VISIBLE
        dialog?.setContentView(view)
        dialog?.show()

        view.bt_add_marker.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                if (TextUtils.isEmpty(view.et_latitude.text)) {
                    view.et_latitude.error = "Please enter a latitude"
                    isValid = false
                }

                if (TextUtils.isEmpty(view.et_longitude.text)) {
                    view.et_latitude.error = "Please enter a longitude"
                    isValid = false
                }

                if (isValid) {
                    destinationDegree = compassViewModel.bearing(compassViewModel.currentLatitude, compassViewModel.currentLongitude, view.et_latitude.text.toString().toDouble(), view.et_longitude.text.toString().toDouble())
                    mBinding.texts.visibility = View.VISIBLE
                    constaint.clone(parents)

                    constaint.constrainCircle(texts.getId(), imageViewCompass.getId(), 360, destinationDegree.toFloat() - currentDegree);
                    constaint.applyTo(parents)
                    dialog?.hide()

                }
            }


        })


    }
    private fun observeApiResponse() {
        compassViewModel.apiResponseLiveData.observe(this, Observer { response ->
            when {
                response?.apiResponse != null -> {
                ll_location.visibility = View.VISIBLE
                    constaint.clone(parents)

                    constaint.constrainCircle(iv_location.getId(), imageViewCompass.getId(), 360, compassViewModel.apiDegree.toFloat() - currentDegree);
                    constaint.applyTo(parents)
                    mBinding.executePendingBindings()
                }
                response?.throwable != null ->
                    if(response.throwable!!.message != null){

                    }
            }
        })
    }



}
