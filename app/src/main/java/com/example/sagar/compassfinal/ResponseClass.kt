package com.example.sagar.compassfinal

class ResponseClass<T> {

    var error: CommonError? = null
    var throwable: Throwable? = null
    var apiResponse: T? = null
}