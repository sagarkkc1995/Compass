package com.example.sagar.compassfinal


data class LocationResponse(
    val lat: Double,
    val long: Double
)