package com.example.sagar.compassfinal

import android.app.Application
import android.arch.lifecycle.AndroidViewModel

open class BaseViewModel(application: Application?) : AndroidViewModel(application) {
}