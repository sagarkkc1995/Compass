package com.example.sagar.compassfinal

/**
 * An Interface to handle api response
 * @author Ranosys technologies
 * @Date 8-Mar-2018
 */

interface ApiCallback<T>{
    fun onException(error : Throwable)

    fun onError(error : CommonError)

    fun onSuccess(t : T?)
}