package com.example.sagar.compassfinal

import android.app.Application
import android.arch.lifecycle.MutableLiveData
import android.databinding.ObservableField
import android.location.Location
import android.widget.Toast

class CompassViewModel(application: Application) : BaseViewModel(application) {

    val appContext: Application = application

    val latitude: ObservableField<String> = ObservableField()
    val longitude: ObservableField<String> = ObservableField()
    val observableDistance: ObservableField<String> = ObservableField()
    val locationResponse: ObservableField<LocationResponse> = ObservableField()
    var currentLatitude: Double = 0.0
    var currentLongitude: Double = 0.0
    var apiDegree : Double = 0.0
    var distance : Float = 0f
    val apiResponseLiveData = MutableLiveData<ResponseClass<LocationResponse>>()


    fun bearing(startLat: Double, startLng: Double, endLat: Double, endLng: Double): Double {
        val latitude1 = Math.toRadians(startLat)
        val latitude2 = Math.toRadians(endLat)
        val longDiff = Math.toRadians(endLng - startLng)
        val y = Math.sin(longDiff) * Math.cos(latitude2)
        val x = Math.cos(latitude1) * Math.sin(latitude2) - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(longDiff)
        return (Math.toDegrees(Math.atan2(y, x)) + 360) % 360
    }


    fun getLocation() {

        var gpsTracker = GpsTracker(appContext)
        if (gpsTracker.canGetLocation()) {
            currentLatitude = gpsTracker.getLatitude()
            currentLongitude = gpsTracker.getLongitude()

            latitude.set(gpsTracker.getLatitude().toString())
            longitude.set(gpsTracker.getLongitude().toString())


        } else {
            gpsTracker.showSettingsAlert()
        }
    }

    fun getLocationApi() {
        val apiResponse: ResponseClass<LocationResponse> = ResponseClass()
        AppRepository.getLocation(object : ApiCallback<LocationResponse> {
            override fun onError(error: CommonError) {
                apiResponse.error = error
                apiResponseLiveData.value = apiResponse
            }

            override fun onSuccess(t: LocationResponse?) {
                if (t != null) {
                    apiResponse.apiResponse = t
                    apiResponseLiveData.value = apiResponse
                    apiDegree = bearing(currentLatitude,currentLongitude,t.lat,t.long)
                    distance = getDistance(currentLatitude,currentLongitude,t.lat,t.long)
                    distance = distance/1000
                    observableDistance.set(distance.toString())
                }
            }

            override fun onException(error: Throwable) {
                apiResponse.throwable = error
                apiResponseLiveData.value = apiResponse
            }

        })
    }


    fun getDistance(currentLatitude: Double, currentLongitude: Double, destinationLat: Double, desinationLong: Double): Float {
        val loc1 = Location("")
        loc1.setLatitude(currentLatitude)
        loc1.setLongitude(currentLongitude)

        val loc2 = Location("")
        loc2.setLatitude(destinationLat)
        loc2.setLongitude(desinationLong)

        val distanceInMeters = loc1.distanceTo(loc2)
        return distanceInMeters
    }


}